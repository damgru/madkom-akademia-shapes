<?php
require "vendor/autoload.php";

use App\Shapes\Infrastructure\StaticShapeFactory;
use App\Shapes\InputData\CsvInputData;
use App\Shapes\Output\EchoOutput;
use App\Shapes\Service\MaxShapeService;

$data = new CsvInputData(file_get_contents("input/test1.csv"));
$service = new MaxShapeService();
$output = new EchoOutput();

$maxFieldShape = $service->getMaxField(...StaticShapeFactory::fromInputData($data));
$maxPerimeterShape = $service->getMaxPerimeter(...StaticShapeFactory::fromInputData($data));
$output->print("{$maxFieldShape->name()} ma największe pole {$maxFieldShape->field()}\n");
$output->print("{$maxPerimeterShape->name()} ma największy obwod {$maxPerimeterShape->perimeter()}\n");
