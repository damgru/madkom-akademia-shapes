<?php
namespace App\Shapes\InputData;

interface InputData
{
    public function asArray(): array;
}