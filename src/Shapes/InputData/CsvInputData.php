<?php
namespace App\Shapes\InputData;

class CsvInputData implements InputData
{
    private $array = [];

    /**
     * CsvInputData constructor.
     */
    public function __construct($textWithData)
    {
        $array = explode("\n", $textWithData);
        foreach ($array as $item) {
            $this->array[] = explode(";", $item);
        }
    }

    public function asArray(): array
    {
        return $this->array;
    }
}