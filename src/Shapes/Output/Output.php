<?php
namespace App\Shapes\Output;

interface Output
{
    public function print($text);
}