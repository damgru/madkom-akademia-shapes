<?php
namespace App\Shapes\Output;

class EchoOutput implements Output
{
    public function print($text)
    {
        echo $text;
    }
}
