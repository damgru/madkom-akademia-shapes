<?php
namespace App\Shapes\Domain\Shape;

interface Shape
{
    public function field(): float;
    public function perimeter(): float;
    public function name(): string ;
}