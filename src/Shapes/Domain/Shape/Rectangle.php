<?php
namespace App\Shapes\Domain\Shape;

class Rectangle implements Shape
{
    private $x,$y;
    private $name;

    /**
     * Square constructor.
     * @param $x
     * @param $y
     * @param string $name
     */
    public function __construct(float $x, float $y, $name = "Prostokat")
    {
        $this->x = $x;
        $this->y = $y;
        $this->name = $name;
    }

    public function field(): float
    {
        return $this->x*$this->y;
    }

    public function perimeter(): float
    {
        return 2*($this->x+$this->y);
    }


    public function name(): string
    {
        return "$this->name($this->x,$this->y)";
    }
}