<?php
namespace App\Shapes\Domain\Shape;

class Elipse implements Shape
{
    private $x,$y;
    const PI = 3.14;
    /** @var string */
    private $name;

    /**
     * Elipse constructor.
     * @param float $x
     * @param float $y
     * @param string $name
     */
    public function __construct(float $x, float $y, string $name = 'Elipsa')
    {
        $this->x = $x;
        $this->y = $y;
        $this->name = $name;
    }

    public function field(): float
    {
        return self::PI*$this->x*$this->y;
    }

    public function perimeter(): float
    {
        return self::PI*((3.0*($this->x+$this->y)/2.0)-sqrt($this->x*$this->y));
    }

    public function name(): string
    {
        return "$this->name($this->x,$this->y)";
    }
}