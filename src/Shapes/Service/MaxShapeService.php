<?php
namespace App\Shapes\Service;
use App\Shapes\Domain\Shape\Shape;

class MaxShapeService
{
    public function getMaxField(Shape ...$shapes): Shape
    {
        $obj = null;
        $max = PHP_FLOAT_MIN;
        foreach ($shapes as $shape) {
            $field = $shape->field();
            if($max < $field) {
                $obj = $shape;
                $max = $field;
            }
        }
        return $obj;
    }

    public function getMaxPerimeter(Shape ...$shapes): Shape
    {
        $obj = null;
        $max = PHP_FLOAT_MIN;
        foreach ($shapes as $shape) {
            $perimeter = $shape->perimeter();
            if($max < $perimeter) {
                $obj = $shape;
                $max = $perimeter;
            }
        }
        return $obj;
    }

}