<?php


namespace App\Shapes\Infrastructure;

class UnknownShapeTypeException extends \Exception
{
    private $row;

    /**
     * UnknownShapeTypeException constructor.
     * @param $row
     */
    public function __construct($row)
    {
        $this->row = $row;
        parent::__construct("UnknownShapeTypeException $row[0]");
    }
}