<?php
namespace App\Shapes\Infrastructure;

use App\Shapes\Domain\Shape\Elipse;
use App\Shapes\Domain\Shape\Shape;
use App\Shapes\Domain\Shape\Rectangle;
use App\Shapes\InputData\InputData;

class StaticShapeFactory
{
    /**
     * @param InputData $data
     * @return Shape[]
     * @throws UnknownShapeTypeException
     */
    public static function fromInputData(InputData $data): iterable
    {
        foreach ($data->asArray() as $row)
        {
            switch ($row[0]) {
                case 'kwadrat':
                case 'prostokat':
                    yield new Rectangle(floatval($row[1]), floatval($row[2]), $row[0]);
                    break;
                case 'kolo':
                    yield new Elipse(floatval($row[1]), floatval($row[1]), $row[0]);
                    break;
                case 'elipsa':
                    yield new Elipse(floatval($row[1]), floatval($row[2]), $row[0]);
                    break;
                default:
                    throw new UnknownShapeTypeException($row);
            }
        }
    }
}